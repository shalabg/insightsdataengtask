import pprint
import sys
import math

recipients = {}
donors = {}

NUM_FIELDS = 21
RECIPIENT_POS   = 0
DONOR_NAME_POS  = 7
DONOR_ZIPCODE_POS  = 10
DT_TRANSACTION_POS = 13
AMT_TRANSACTION_POS = 14
OTH_TRANSACTION_POS = 15

# calculate rank
def calculateRank(list, percentile):
    calculateOrdinal = int(math.ceil((percentile/100.0) * len(list)));
    return calculateOrdinal - 1

# add element in sorted order
def addElementInOrder(list, elem):
    list.append(elem)
    length = len(list)
    i = length-1;
    # move this item to appropriate location
    while (i > 0):
        if list[i] <= list[i-1]:
            swap = list[i-1]
            list[i-1] = list[i]
            list[i] = swap
        i = i - 1

if len(sys.argv) != 4:
    print "usage: donation_analytics input.txt percentile.txt output.txt"
    sys.exit()

INPUT_FILE = sys.argv[1]
PERCENTILE_FILE = sys.argv[2]
OUTPUT_FILE = sys.argv[3]

pp = pprint.PrettyPrinter(indent=4)
filePercentile = open(PERCENTILE_FILE, "r")
fileIn = open(INPUT_FILE,"r")
fileOut = open(OUTPUT_FILE, "w")

percentile = filePercentile.readline().strip();
try:
    percentile = int(percentile)
except ValueError:
    print "percentile should be numeric value"
    sys.exit()

for line in fileIn:
  
  #Let's split the line into an array called "fields" using the ";" as a separator:
  fields = line.split("|")
  
  # line does not contain sufficient nuber of fields
  if len(fields) != NUM_FIELDS:
    continue;

  # and let's extract the data:
  recipient = fields[RECIPIENT_POS].strip()
  if len(recipient)  == 0:
    continue

  donor = fields[DONOR_NAME_POS].strip()
  if len(donor)  == 0:
    continue

  # clean up zip code for first five, if not valid continue without processing
  zipcode = fields[DONOR_ZIPCODE_POS].strip()
  if len(zipcode)  < 5:
    continue

  zipcode = zipcode[:5]

  # clean up date for year, if not valid continue without processing
  dateyear = fields[DT_TRANSACTION_POS].strip()
  if (len(dateyear) != 8):
    continue
  dateyear = dateyear[4:]


  amount = fields[AMT_TRANSACTION_POS].strip()
  if len(amount)  == 0:
    continue

  #OTHER_ID field contains any other value, you should completely ignore
  other = fields[OTH_TRANSACTION_POS]
  if len(other)  != 0:
    continue

  # failed to get proper value for amount or year, continue without processing
  try:
    amount = int(amount)
    dateyear = int(dateyear)
  except ValueError:
    continue

  donorkey = donor + "_" + zipcode;
  if donorkey not in donors:
 	donors[donorkey] = dateyear
  else:
    if donors[donorkey] >= dateyear:
        donors[donorkey] = dateyear
        continue
    
    if recipient not in recipients:
      recipients[recipient] = {}
      recipients[recipient][dateyear] = []
    else:
      if dateyear not in recipients[recipient]:
        recipients[recipient][dateyear] = []

    recipientYearAmountList  = recipients[recipient][dateyear]
    addElementInOrder(recipientYearAmountList, amount)  #add to the list
    lengthOfList = len(recipientYearAmountList)
    fileOut.write(recipient + "|" + zipcode + "|" + str(dateyear) + "|" +
                  str(recipientYearAmountList[calculateRank(recipientYearAmountList, percentile)]) + "|" +
                  str(sum(recipientYearAmountList)) + "|" + str(lengthOfList) + "\n");

fileIn.close()
fileOut.close()


